package uvsq.m1info.roguelike.agents;

public class MimicAi extends CreatureAi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1889528373937177964L;
	private Creature target;
	private int cpt = 0;
	public MimicAi(Creature creature, Creature player) {
		super(creature);
		this.target = player;
	}

	public void onUpdate(){
		cpt = (cpt + 1)%3;
		if (cpt == 0) {
			hunt(target);
		}
	}
}	
